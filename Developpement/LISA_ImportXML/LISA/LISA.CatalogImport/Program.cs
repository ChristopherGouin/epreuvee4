﻿
using LISA.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LISA.CatalogImport
{
    class Program
    {
        public const string ARGS_HELP = "/?";
        public const string ARGS_DIRECTORY = "/d";

        static void Main(string[] args)
        {
            // Arguments
            // /d <Dossier a vérifier> : Préciser le dossier a vérifier pour l'import
            // /? : affiche la documentation

            if (args.Length == 2 && args[0] == ARGS_DIRECTORY)
            {
 
               
                //Sytem.IO.Directory.Exists(<chemein du dossier>)

                if (Directory.Exists(args[1]))
                {
                    
                    //Sytem.IO.Directory.EnumerateDirectories()

                    

                    foreach (var directory in Directory.EnumerateDirectories(args[1], "*", SearchOption.TopDirectoryOnly))
                    {
                        //Affiche les sous-dossiers
                        //Console.WriteLine(directory);

                        // TODO : Rechercher dans chaque dossier s'il existe un fichier "*.xml". Afficher une erreur si ce n'est pas le cas
                        //Sytem.IO.Directory.Enumeratefiles()

                        IEnumerable<string> listXMLFiles = Directory.EnumerateFiles(directory, "*.xml", SearchOption.TopDirectoryOnly);


                        if (listXMLFiles.Count() == 1)
                        {
                            // Appeler la fonction ImportXMLFile pour chaque fichier *.xml
                            string filePath = listXMLFiles.First();

                            ImportXMLFile(filePath);
                        }
                        else
                        {
                            
                            Console.WriteLine($"Il n'existe aucun fichier XML dans le dossier {directory}");
                        }

                        //Directory.Delete(directory, true);
                        
                    }
                }
                else
                {
                    Console.WriteLine($"Le dossier {args[1]} n'existe pas");
                }








            }
            else
            {
                // On affiche la documentation
                Console.WriteLine(ARGS_DIRECTORY + " <Dossier a vérifier> : Préciser le dossier a vérifier pour l'import");
                Console.WriteLine(ARGS_HELP + " : affiche la documentation");


                Console.WriteLine("LISA.CatalogImport.exe " + ARGS_HELP);
                Console.WriteLine(" \t\t affiche la documentation");

                Console.WriteLine("LISA.CatalogImport.exe " + ARGS_DIRECTORY + " : <chemin du dossier>");
                Console.WriteLine(" \t\t Importe les Catalogss dans le repertoire spécifié");


            }

#if DEBUG
            Console.WriteLine("\n Appuyer sur une touche pour quitter ...");
            Console.ReadKey();
#endif



        }
        

        /// <summary>
        /// Import of th xml file
        /// </summary>
        /// <param name="filePath"></param>
        private static void ImportXMLFile(string filePath)
        {
            
            Console.WriteLine($"Import de fichier : {filePath}");// nouvelle syntaxe du string.format depuis C# 6


            try
            {
                using (LISAEntities entities = new LISAEntities())
                {
                    
                    entities.Database.Connection.Open();

                    
                    XDocument document = XDocument.Load(filePath);

                   
                    foreach (XElement OperationsElement in document.Descendants(XName.Get("operation")))
                    {
                        
                        // parsing of the Operations
                        Operations Operations = ParseOperationsElement(OperationsElement, entities);
                        
                        
                        foreach (XElement catalogElement in OperationsElement.Elements(XName.Get("catalog")))
                        {
                            // parsing of the catalog
                            Catalogs Catalogs = ParseCatalogsElement(catalogElement, entities, Operations);

                           
                            foreach (XElement shopElement in catalogElement.Elements(XName.Get("shops")).Elements())
                            {
                                //Prasing of the shop
                                ParseMagasinElement(shopElement, entities, Catalogs);
                            }

                            
                            foreach (XElement PagesElement in catalogElement.Elements(XName.Get("pages")).Elements())
                            {
                                // parsing of the Pages
                                Pages Pages = ParsePagesElement(PagesElement, entities, Catalogs);

                                
                                foreach (XElement ProductsElement in PagesElement.Elements(XName.Get("products")).Elements())
                                {
                                    //Parsing of the Products
                                    Products Products = ParseProductsElement(ProductsElement, entities);

                                    
                                    ParsePrixCatalogsProductsElement(ProductsElement, entities, Catalogs, Products);
                                    ParsePagesProductsElement(ProductsElement, entities, Pages, Products);

                                    //parsing of the media
                                    ParseMediaElement(ProductsElement, entities, Products, "image");
                                    ParseMediaElement(ProductsElement, entities, Products, "picto");
                                    

                                }

                            }

                        }


                    }

                    
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                // TODO : voir comment gérer les erreurs d'import
                Console.WriteLine($"Impossible d'importer le fichier {filePath + Environment.NewLine + ex}");
            }

        }


        /// <summary>
        /// parsing of the images and the pictogram
        /// </summary>
        /// <param name="ProductsElement"></param>
        /// <param name="entities"></param>
        /// <param name="Products"></param>
        /// <param name="baliseTypeMedia"></param>
        private static void ParseMediaElement(XElement ProductsElement, LISAEntities entities, Products Products, string baliseTypeMedia)
        {
            Media media = null;

            TypeMedia resultTypeMedia = null;
            
            // Searching if this typeMedia is allready in DB
            resultTypeMedia = entities.TypeMedia.FirstOrDefault(tm => tm.Label == baliseTypeMedia);

            if (resultTypeMedia == null)
            {
                // If is not in DB, verify if it will be added
                DbEntityEntry<TypeMedia> entityTypeMedia = entities.ChangeTracker.Entries<TypeMedia>().ToList()
                    .FirstOrDefault(tm => tm.State.ToString() == "Added" && tm.Entity.Label == baliseTypeMedia);

                if (entityTypeMedia == null)
                {
                    // if it won't be, parsing it
                    resultTypeMedia = new TypeMedia()
                    {
                        Label = baliseTypeMedia
                    };

                    entities.TypeMedia.Add(resultTypeMedia);

                }
                else
                {
                    resultTypeMedia = entityTypeMedia.Entity;
                }
            }
            

            string mediaChemin = ProductsElement.Element(XName.Get(baliseTypeMedia)).Value;

            string[] listChemin = mediaChemin.Split(',');

            // parsing of the media
            foreach (string chemin in listChemin)
            {
                media = new Media()
                {
                    Products = Products,
                    TypeMedia = resultTypeMedia,
                    Link = chemin.Trim()
                };
                entities.Media.Add(media);

            }

            


        }

        /// <summary>
        /// parsing of th coordonne of the Products
        /// </summary>
        /// <param name="ProductsElement"></param>
        /// <param name="entities"></param>
        /// <param name="Pages"></param>
        /// <param name="Products"></param>
        private static void ParsePagesProductsElement(XElement ProductsElement, LISAEntities entities, Pages Pages, Products Products)
        {
            int zoneHauteur = int.Parse(ProductsElement.Element(XName.Get("zones")).Element("zone").Element(XName.Get("height")).Value);
            int zoneLargeur = int.Parse(ProductsElement.Element(XName.Get("zones")).Element("zone").Element(XName.Get("width")).Value);
            int zoneCoordx = int.Parse(ProductsElement.Element(XName.Get("zones")).Element("zone").Element(XName.Get("coordx")).Value);
            int zoneCoordy = int.Parse(ProductsElement.Element(XName.Get("zones")).Element("zone").Element(XName.Get("coordy")).Value);

            //Searching if this PagesProducts allready exist in DB
            PagesProducts result = entities.PagesProducts.FirstOrDefault(p => p.Id_Products == Products.Id && p.Id_Pages == Pages.Id);

            if (result != null)
            {
                entities.PagesProducts.Remove(result);
            }
            
            result = new PagesProducts()
            {
                Pages = Pages,
                Products = Products,
                Heigth = zoneHauteur,
                Width = zoneLargeur,
                Coordx= zoneCoordx,
                Coordy= zoneCoordy
            };
            entities.PagesProducts.Add(result);
        }


        /// <summary>
        /// Parsing of the price in relation of a Catalogs and a Products
        /// </summary>
        /// <param name="ProductsElement"></param>
        /// <param name="entities"></param>
        /// <param name="Catalogs"></param>
        /// <param name="Products"></param>
        private static void ParsePrixCatalogsProductsElement(XElement ProductsElement, LISAEntities entities, Catalogs Catalogs, Products Products)
        {
            decimal prixCA = decimal.Parse(ProductsElement.Element(XName.Get("price")).Value) / 100;

            decimal prixAvantCouponCA = 0;
            if (decimal.TryParse(ProductsElement.Element(XName.Get("price_before_coupon")).Value, out prixAvantCouponCA))
            {
                prixAvantCouponCA = prixAvantCouponCA / 100;
            };
            
            decimal prixAvantCroiseCA = 0;
            if (decimal.TryParse(ProductsElement.Element(XName.Get("price_crossed")).Value, out prixAvantCroiseCA))
            {
                prixAvantCroiseCA = prixAvantCroiseCA / 100;
            }

            decimal reducEuroCA = 0;
            if (decimal.TryParse(ProductsElement.Element(XName.Get("Reduction_euro")).Value, out reducEuroCA))
            {
                reducEuroCA = reducEuroCA / 100;
            }

            decimal reducPourcentCA = 0;
            decimal.TryParse(ProductsElement.Element(XName.Get("Reduction_percent")).Value, out reducPourcentCA);

            decimal aventageEuroCA = 0;
            if (decimal.TryParse(ProductsElement.Element(XName.Get("Avantage_euro")).Value, out aventageEuroCA))
            {
                aventageEuroCA = aventageEuroCA / 100;
            }

            decimal avantagePourcentCA = 0;
            decimal.TryParse(ProductsElement.Element(XName.Get("Avantage_percent")).Value, out avantagePourcentCA);

            decimal ecoTaxe = 0;
            if (decimal.TryParse(ProductsElement.Element(XName.Get("ecotaxe")).Value, out ecoTaxe))
            {
                ecoTaxe = ecoTaxe / 100;
            }

            decimal PrixBas = 0;
            if (decimal.TryParse(ProductsElement.Element(XName.Get("lowerprice")).Value, out PrixBas))
            {
                PrixBas = PrixBas / 100;
            }


            // Searching if this PrixCatalogsProducts allready exists in DB
            PrixCatalogs result = entities.PrixCatalogs.FirstOrDefault(p => p.Id_Products == Products.Id && p.Id_Catalogs == Catalogs.Id);

            if (result != null)
            {
                entities.PrixCatalogs.Remove(result);
            }

            result = new PrixCatalogs
            {
                Catalogs = Catalogs,
                Products = Products,
                Price = prixCA,
                PriceBeforeCoupon = prixAvantCouponCA,
                PriceCrossed = prixAvantCroiseCA,
                ReductionEuro = reducEuroCA,
                ReductionPourcent = reducPourcentCA,
                AvantageEuro = aventageEuroCA,
                AvantagePourcent = avantagePourcentCA,
                Ecotaxe = ecoTaxe,
                Lowerprice = PrixBas
            };
            entities.PrixCatalogs.Add(result);
            

        }


        /// <summary>
        /// Parsing of an Products
        /// </summary>
        /// <param name="ProductsElement"></param>
        /// <param name="entities"></param>
        /// <param name="PagesElement"></param>
        /// <returns>Products</returns>
        private static Products ParseProductsElement(XElement ProductsElement, LISAEntities entities)
        {
            Products result = null;

            
            int ProductsId = int.Parse(ProductsElement.Attribute(XName.Get("id")).Value);
            int ProductsCode = int.Parse(ProductsElement.Element(XName.Get("code")).Value);
            string ProductsLibelle = ProductsElement.Element(XName.Get("label")).Value;
            string ProductsDescritpion = ProductsElement.Element(XName.Get("description")).Value;
            string ProductsUnite = ProductsElement.Element(XName.Get("packaging")).Value;
            string ProductsMention = ProductsElement.Element(XName.Get("mention")).Value;
            string ProductsColor = ProductsElement.Element(XName.Get("color")).Value;
            string ProductsOrigin = ProductsElement.Element(XName.Get("origin")).Value;
            Nullable<DateTime> ProductsStartDate = null;
            double testDouble;
            if (double.TryParse(ProductsElement.Element(XName.Get("start_validity_date")).Value, out testDouble))
            {
                ProductsStartDate = UnixTimeStampToDateTime(double.Parse(ProductsElement.Element(XName.Get("start_validity_date")).Value));
            }

            Nullable<DateTime> ProductsEndDate = null;
            if (double.TryParse(ProductsElement.Element(XName.Get("end_validity_date")).Value, out testDouble))
            {
                ProductsEndDate = UnixTimeStampToDateTime(double.Parse(ProductsElement.Element(XName.Get("end_validity_date")).Value));
            }


            Categories categorie = ParseCategorieElement(ProductsElement, entities);

            result = entities.Products.FirstOrDefault(a => a.ImportId == ProductsId);

            if (result != null)
            {
                entities.Products.Remove(result);
            }

            result = new Products
            {
                ImportId = ProductsId,
                Code = ProductsCode,
                Label = ProductsLibelle,
                Description = ProductsDescritpion,
                Packaging = ProductsUnite,
                Mention = ProductsMention,
                Color = ProductsColor,
                StartValidityDate = ProductsStartDate,
                EndValidityDate = ProductsEndDate,
                Origin=ProductsOrigin,
                Categories = categorie
            };

            entities.Products.Add(result);


            return result;
        }

        /// <summary>
        /// Parsing of a cateory
        /// </summary>
        /// <param name="ProductsElement"></param>
        /// <param name="entities"></param>
        /// <returns>Categorie</returns>
        private static Categories ParseCategorieElement(XElement ProductsElement, LISAEntities entities)
        {
            Categories result = null;

            string categorieLibelle = ProductsElement.Element(XName.Get("category")).Value;

            // Searching if this category allready exist in DB
            result = entities.Categories.FirstOrDefault(c => c.Label == categorieLibelle);

            if (result == null)
            {
                // If doesn't exist, searching if it will be add in DB
                DbEntityEntry<Categories> entityCategorie = entities.ChangeTracker.Entries<Categories>().ToList()
                    .FirstOrDefault(c => c.State.ToString() == "Added" && c.Entity.Label == categorieLibelle);

                if (entityCategorie == null)
                { 
                    // if it won't be add, parsing it in DB
                    result = new Categories()
                    {
                        Label= categorieLibelle
                    };

                    entities.Categories.Add(result);
                }
                else
                {
                    result = entityCategorie.Entity;
                }

            }
           

            return result;

        }

        /// <summary>
        /// Parsing of an opperation
        /// </summary>
        /// <param name="OperationsElement"></param>
        /// <param name="entities"></param>
        /// <returns>Operations</returns>
        private static Operations ParseOperationsElement(XElement OperationsElement, LISAEntities entities)

        {
            Operations result = null;

            // recuperer les données de l'opération dans le fichier XML
            int OperationsId = int.Parse(OperationsElement.Attribute(XName.Get("id")).Value);
            string OperationsCode = OperationsElement.Element(XName.Get("code")).Value;
            string OperationsTitle = OperationsElement.Element(XName.Get("title")).Value;
            DateTime OperationsStartDate = UnixTimeStampToDateTime(double.Parse(OperationsElement.Element(XName.Get("startDate")).Value));
            DateTime OperationsEndDate =UnixTimeStampToDateTime(double.Parse(OperationsElement.Element(XName.Get("endDate")).Value));



            // Verifier si l'op n'existe pas déja en base

            result = entities.Operations.FirstOrDefault(op => op.ImportId == OperationsId);

            // créer l'op
            if (result != null)
            {
                entities.Operations.Remove(result);
            }

            result = new Operations
            {
                ImportId = OperationsId,
                Code = OperationsCode,
                Title = OperationsTitle,
                StartDAte = OperationsStartDate,
                EndDate = OperationsEndDate
            };
            

            entities.Operations.Add(result);

            return result;
        }


        /// <summary>
        /// Parsing of a Catalog
        /// </summary>
        /// <param name="catalogElement"></param>
        /// <param name="entities"></param>
        /// <param name="Operations"></param>
        /// <returns>Catalogs</returns>
        private static Catalogs ParseCatalogsElement(XElement catalogElement, LISAEntities entities, Operations Operations)
        {
            Catalogs result = new Catalogs();

            int CatalogsId = int.Parse(catalogElement.Attribute(XName.Get("id")).Value);
            String CatalogsType = catalogElement.Element(XName.Get("type")).Value;
            String catalouegLabel = catalogElement.Element(XName.Get("label")).Value;
            String CatalogsSpeed = catalogElement.Element(XName.Get("speed")).Value;
            int CatalogsWidth = int.Parse(catalogElement.Element(XName.Get("catalogWidth")).Value);
            int CatalogsHeight = int.Parse(catalogElement.Element(XName.Get("catalogHeight")).Value);

            result = entities.Catalogs.FirstOrDefault(cat => cat.ImportId == CatalogsId);

            if (result != null)
            {
                entities.Catalogs.Remove(result);
            }

            result = new Catalogs()
            {
                ImportId = CatalogsId,
                Type = CatalogsType,
                Label = catalouegLabel,
                Speed = CatalogsSpeed,
                Width = CatalogsWidth,
                Height = CatalogsHeight,
                Operations = Operations
            };

            entities.Catalogs.Add(result);

            return result;
            
        }


        /// <summary>
        /// Parsing of a Pages
        /// </summary>
        /// <param name="PagesElement"></param>
        /// <param name="entities"></param>
        /// <param name="Catalogs"></param>
        /// <returns>Pages</returns>
        private static Pages ParsePagesElement(XElement PagesElement, LISAEntities entities, Catalogs Catalogs)
        {

            int PagesId = int.Parse(PagesElement.Attribute(XName.Get("id")).Value);
            int PagesNumber = int.Parse(PagesElement.Element(XName.Get("number")).Value);

            Pages Pages = entities.Pages.FirstOrDefault(p => p.ImportId == PagesId);

            if (Pages != null)
            {
                entities.Pages.Remove(Pages);
            }

            Pages = new Pages
            {
                ImportId = PagesId,
                Number = PagesNumber,
                Catalogs = Catalogs
            };

            entities.Pages.Add(Pages);

            return Pages;
        }


        /// <summary>
        /// Parsing of the shop (+ shopCatalog)
        /// </summary>
        /// <param name="shopElement"></param>
        /// <param name="entities"></param>
        /// <param name="Catalogs"></param>
        private static void ParseMagasinElement(XElement shopElement, LISAEntities entities, Catalogs Catalogs)
        {
            int shopId = int.Parse(shopElement.Attribute(XName.Get("id")).Value);

            Shops result = entities.Shops.FirstOrDefault(mag => mag.ImportId == shopId);

            if (result == null)
            {

                result = new Shops
                {
                    ImportId = shopId,
                    Label = shopId.ToString()
                };

                entities.Shops.Add(result);

            }

            DateTime shopStartDate = UnixTimeStampToDateTime(double.Parse(shopElement.Element(XName.Get("startDate")).Value));
            DateTime shopDisplayStartDate = UnixTimeStampToDateTime(double.Parse(shopElement.Element(XName.Get("displayStartDate")).Value));
            DateTime shopDisplayEndDate = UnixTimeStampToDateTime(double.Parse(shopElement.Element(XName.Get("displayEndDate")).Value));

            ShopsCatalogs magasinCatalog = entities.ShopsCatalogs.FirstOrDefault(mg => mg.Id_Catalogs == Catalogs.ImportId && mg.Id_Shops == shopId);

            if (magasinCatalog != null)
            {
                entities.ShopsCatalogs.Remove(magasinCatalog);
            }

            magasinCatalog = new ShopsCatalogs
            {
                StartDate = shopDisplayStartDate,
                EndDate = shopDisplayEndDate,
                Catalogs = Catalogs,
                Shops = result
            };

            entities.ShopsCatalogs.Add(magasinCatalog);


        }


        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {


            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(unixTimeStamp).ToLocalTime();

        }
    }

}
