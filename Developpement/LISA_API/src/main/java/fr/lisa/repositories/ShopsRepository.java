package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.Shops;
@Repository
public interface ShopsRepository extends CrudRepository<Shops, Integer>{

    public List<Shops> findAll();

    public Shops findById(Integer Id);

    public List<Shops> findByLabelContains(String label);

    public  Shops findByImportid(Integer importid);

}
