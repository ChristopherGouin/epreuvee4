package fr.lisa.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.Operations;

@Repository
public interface OperationsRepository extends CrudRepository<Operations, Integer>{

    public Operations findById(Integer id);

    public Operations findOne(Integer id);

    public List<Operations> findAll();

    public List<Operations> findByCodeContaining(String code);

    public List<Operations> findByStartdateBeforeAndEnddateAfter(Date sdate, Date edate);
}
