package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.TypeMedia;
@Repository
public interface TypeMediaRepository extends CrudRepository<TypeMedia, Integer>{

    public List<TypeMedia> findAll();

    public TypeMedia findById(Integer Id);

    public List<TypeMedia> findByLabelContains(String labal);
}
