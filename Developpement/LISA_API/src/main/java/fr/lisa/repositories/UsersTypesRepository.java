package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.UsersTypes;

@Repository
public interface UsersTypesRepository extends CrudRepository<UsersTypes, Integer> {

    public List<UsersTypes> findAll();

    public UsersTypes findById(Integer Id);

    public List<UsersTypes> findByLabelContains(String label);
}
