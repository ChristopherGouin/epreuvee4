package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.Catalogs;
import fr.lisa.entities.Products;
@Repository
public interface ProductsRepository extends CrudRepository<Products, Integer>{

    public List<Products> findAll();

    public Products findById(Integer Id);

    public List<Products> findByCategories_Id(Integer categoryid);

    public Products findByCode(Integer code);

    public List<Products> findByLabelContains(String label);


    /**
     * Find products for a catalog
     * @return
     */
    @Query("SELECT products FROM Products products " +
            "JOIN products.prixcatalogs prixcatalogs " +
            "JOIN prixcatalogs.catalogs catalogs " +
            "WHERE catalogs.id = :Id")
    public List<Products> findByCatalog(@Param("Id") Integer Id);
}
