package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.Users;

@Repository
public interface UsersRepository extends CrudRepository<Users, Integer>{

    public List<Users> findAll();

    public Users findById(Integer Id);

    public List<Users> findByLoginContains(String login);

    public List<Users> findByUserstypes_Id(Integer userstypesid);

    public Users findByLoginAndPassword(String login, String password);

    public Users findByToken(String token);

    public Users findByLoginAndPasswordAndToken(String login, String password, String token);

}
