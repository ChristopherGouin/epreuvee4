package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.ShopsCatalogs;

@Repository
public interface ShopsCatalogsRepository extends CrudRepository<ShopsCatalogs, Integer> {


    public List<ShopsCatalogs> findAll();

    public List<ShopsCatalogs> findByShops_Id(Integer shopid);

    public List<ShopsCatalogs> findByCatalogs_Id(Integer catalogid);

    public ShopsCatalogs findByShops_IdAndCatalogs_Id(Integer shopid, Integer catalogid);
}
