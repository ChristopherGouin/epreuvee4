package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.Applications;

@Repository
public interface ApplicationsRepository extends CrudRepository<Applications, Integer> {

    /**
     * Find all applications
     */
    public List<Applications> findAll();

    /**
     * find the application with his id
     * @param id
     * @return
     */
    public Applications findById(Integer id);


    /**
     * Find applications by label
     * @param label
     * @return
     */
    public List<Applications> findByLabelContains(String label);
}
