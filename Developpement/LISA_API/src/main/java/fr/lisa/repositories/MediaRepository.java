package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import fr.lisa.entities.Media;

@Repository
public interface MediaRepository extends CrudRepository<Media, Integer> {

    public List<Media> findAll();

    public Media findById(Integer id);

    public List<Media> findByProducts_Id(Integer productid);

    public List<Media> findByTypemedia_Id(Integer typemediaid);

    public List<Media> findByProducts_IdAndTypemedia_Id(Integer productid, Integer typemediaid);


}
