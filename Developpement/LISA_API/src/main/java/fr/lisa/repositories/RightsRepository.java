package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import fr.lisa.entities.Applications;
import fr.lisa.entities.Rights;
import fr.lisa.entities.Users;
@Repository
public interface RightsRepository extends CrudRepository<Rights, Integer>{

    public List<Rights> findAll();

    public Rights findById( Integer Id);

    public List<Rights> findByApplications_Id(Integer applicationid);

    public List<Rights> findByUsers_Id(Integer userid);

    public List<Rights> findByApplications_IdAndUsers_Id(Integer appid, Integer usersid);

}
