package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import fr.lisa.entities.Pages;
@Repository
public interface PagesRepository extends CrudRepository<Pages, Integer> {

    public List<Pages> findAll();

    public Pages findById(Integer Id);

    public List<Pages> findByCatalogs_Id(Integer catalogId);

    public Pages findByCatalogs_IdAndNumber(Integer catalogId, Integer number);
}
