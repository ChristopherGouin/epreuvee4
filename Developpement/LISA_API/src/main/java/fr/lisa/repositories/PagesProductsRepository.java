package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.PagesProducts;

@Repository
public interface PagesProductsRepository extends CrudRepository<PagesProducts, Integer>{

    public List<PagesProducts> findAll();

    public PagesProducts findByPages_IdAndProducts_Id(Integer pagesid, Integer productid);
}
