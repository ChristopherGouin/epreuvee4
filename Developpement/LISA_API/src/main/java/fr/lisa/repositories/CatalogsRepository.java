package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.Catalogs;


@Repository
public interface CatalogsRepository extends CrudRepository<Catalogs, Integer> {

    /**
     * find all catalogs
     */
    public List<Catalogs> findAll();

    /**
     * Find catalog by his Id
     * @param Id
     * @return
     */
    public Catalogs findById(Integer Id);

    /**
     * Find catalogs by Operation Id
     * @param operationid
     * @return
     */
    public List<Catalogs> findByOperations_Id(Integer operationid);


    /**
     * Find catalogs for a user
     * @return
     */
    @Query("SELECT DISTINCT Catalogs FROM Catalogs Catalogs " +
            "JOIN Catalogs.shopsCatalogs shopsCatalogs " +
            "JOIN shopsCatalogs.shops shops " +
            "JOIN shops.users users " +
            "WHERE users.id = :userId")
    public List<Catalogs> findByUser(@Param("userId") Integer userId);

}
