package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import fr.lisa.entities.PrixCatalogs;

@Repository
public interface PrixCatalogsRepository extends CrudRepository<PrixCatalogs, Integer>{

    public List<PrixCatalogs> findAll();

    public List<PrixCatalogs> findByProducts_Id(Integer productid);

    public List<PrixCatalogs> findByCatalogs_Id(Integer catalogid);

    public PrixCatalogs findByProducts_IdAndCatalogs_Id(Integer productid,Integer catalogid);
}
