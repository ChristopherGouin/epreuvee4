package fr.lisa.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lisa.entities.Categories;
@Repository
public interface CategoriesRepository extends CrudRepository<Categories, Integer>{

    public List<Categories> findAll();

    public Categories findById(Integer id);

    public List<Categories> findByLabelContains(String label);
}
