package fr.lisa.security;


import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.servlet4preview.http.HttpFilter;
import org.apache.coyote.http2.Http2Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import fr.lisa.entities.Users;
import fr.lisa.repositories.UsersRepository;

/**
 * @author Christopher
 *
 */
public class SessionManager implements HandlerInterceptor{

    // DB Managers for users and rights
    @Autowired
    UsersRepository userRepo;


    // This method is called before the controller
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        Boolean result = false;
        // Verify if we are not in token checking
        if (request.getRequestURI().contains("bytoken")) {
            result = true;
        }
        if (!result) {

            // Get the token
            String token = request.getParameter("token");
            if (token != null && token != "") {

                Users user = userRepo.findByToken(token);

                result = (user != null);

            }
            if (!result) {
                try {
                    synchronized (response) {
                        response.setStatus(HttpStatus.UNAUTHORIZED.value());
                        response.notify();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return result;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {

    }
}

