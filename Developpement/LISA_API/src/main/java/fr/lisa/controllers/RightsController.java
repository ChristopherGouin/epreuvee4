package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.Rights;
import fr.lisa.repositories.RightsRepository;

@CrossOrigin
@RestController
@RequestMapping("/rights")
public class RightsController {

    @Autowired
    public RightsRepository repo;

    /**
     * Method to get all rights
     * @return
     */
    @RequestMapping("/all")
    public List<Rights> getall(){
        return repo.findAll();
    }

    /**
     * Method to get right by id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public Rights getById(@RequestParam(value = "id") Integer id) {
        return repo.findById(id);
    }

    /**
     * Method to get rights by applications Id
     * @param appid
     * @return
     */
    @RequestMapping("/byapp")
    public List<Rights> getByAppId(@RequestParam(value = "appid") Integer appid){
        return repo.findByApplications_Id(appid);
    }

    /**
     * Method to get rights by user id
     * @param userid
     * @return
     */
    @RequestMapping("/byuser")
    public List<Rights> getByUserIs(@RequestParam(value = "userid") Integer userid){
        return repo.findByUsers_Id(userid);
    }

    /**
     * Method to get rights by application id and user id
     * @param appid
     * @param userid
     * @return
     */
    @RequestMapping("/byappanduser")
    public List<Rights> getByAppAndUser(@RequestParam(value = "appid") Integer appid,
                                        @RequestParam(value = "userid") Integer userid){
        return repo.findByApplications_IdAndUsers_Id(appid, userid);
    }
}
