package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.Catalogs;
import fr.lisa.repositories.CatalogsRepository;

@CrossOrigin
@RestController
@RequestMapping("/catalog")
public class CatalogsController {

    @Autowired
    public CatalogsRepository repo;

    /**
     * Method to get all catalogs
     * @return
     */
    @RequestMapping("/all")
    public List<Catalogs> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get catalog by id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public Catalogs getById(@RequestParam(value = "id") Integer id) {
        return repo.findById(id);
    }

    /**
     * Method to get catalogs by operation id
     * @param opid
     * @return
     */
    @RequestMapping("/byop")
    public List<Catalogs> getByOp(@RequestParam(value = "opid") Integer opid){
        return repo.findByOperations_Id(opid);
    }

    /**
     * Method to get catalogs by user id
     * @param userid
     * @return
     */
    @RequestMapping("/byuser")
    public List<Catalogs> getByUser(@RequestParam(value = "userid") Integer userid){
        return repo.findByUser(userid);
    }
}
