package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.Applications;
import fr.lisa.repositories.ApplicationsRepository;

@CrossOrigin
@RestController
@RequestMapping("/app")
public class ApplicationsController {

    @Autowired
    ApplicationsRepository repo;


    /**
     * Method to get all application
     * @return
     */
    @RequestMapping("/all")
    public List<Applications> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get application by id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public Applications getById(@RequestParam(value = "id") Integer id) {
        return repo.findOne(id);
    }


    /**
     * Method to get application by label
     * @param label
     * @return
     */
    @RequestMapping("/bylab")
    public List<Applications> getByLabel(@RequestParam(value = "label") String label){
        return repo.findByLabelContains(label);
    }

}
