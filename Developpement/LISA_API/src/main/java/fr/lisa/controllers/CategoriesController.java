package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.Categories;
import fr.lisa.repositories.CategoriesRepository;

@CrossOrigin
@RestController
@RequestMapping("/category")
public class CategoriesController {

    @Autowired
    public CategoriesRepository repo;

    /**
     * Method to get all categories
     * @return
     */
    @RequestMapping("/all")
    public List<Categories> getAll(){
        return repo.findAll();
    }

    /**
     * Method t get a category by id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public Categories getById(@RequestParam(value = "id") Integer id) {
        return repo.findById(id);
    }

    /**
     * Method to get categories by label
     * @param label
     * @return
     */
    @RequestMapping("/bylab")
    public List<Categories> getByLabel(@RequestParam(value = "label") String label){
        return repo.findByLabelContains(label);
    }
}
