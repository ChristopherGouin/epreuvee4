package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.PagesProducts;
import fr.lisa.repositories.PagesProductsRepository;

@CrossOrigin
@RestController
@RequestMapping("/pagesproducts")
public class PagesProductsController {

    @Autowired
    public PagesProductsRepository repo;

    /**
     * Method to get all pagesProducts
     * @return
     */
    @RequestMapping("/all")
    public List<PagesProducts> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get pagesProducts by page id and product id
     * @param pagid
     * @param proid
     * @return
     */
    @RequestMapping("/one")
    public PagesProducts getByPageAndProduct(@RequestParam(value = "pagid") Integer pagid,
                                                    @RequestParam(value = "proid") Integer proid){
        return repo.findByPages_IdAndProducts_Id(pagid, proid);
    }
}
