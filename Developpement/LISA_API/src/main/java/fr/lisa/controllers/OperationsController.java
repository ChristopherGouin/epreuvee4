package fr.lisa.controllers;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.Operations;
import fr.lisa.repositories.OperationsRepository;


@CrossOrigin
@RestController
@RequestMapping("/op")
public class OperationsController {

    @Autowired
    private OperationsRepository repo;

    /**
     * Method to get all the operations.
     * @return
     */
    @RequestMapping("/all")
    public List<Operations> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get Operation by Id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public Operations getById(@RequestParam(value = "id") Integer id) {

        return repo.findById(id);
    }


    /**
     * Method to get Operations By code
     * @param code
     * @return
     */
    @RequestMapping("/bycode")
    public List<Operations> getByCode(@RequestParam(value = "code") String code){
        return repo.findByCodeContaining(code);
    }



}
