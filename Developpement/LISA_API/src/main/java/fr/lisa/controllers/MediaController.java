package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.Media;
import fr.lisa.repositories.MediaRepository;

@CrossOrigin
@RestController
@RequestMapping("/media")
public class MediaController {

    @Autowired
    public MediaRepository repo;

    /**
     * Method to get all media
     * @return
     */
    @RequestMapping("/all")
    public List<Media> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get a media by Id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public Media getById(@RequestParam(value = "id") Integer id) {
        return repo.findById(id);
    }

    /**
     * Method to get media by product id
     * @param prodid
     * @return
     */
    @RequestMapping("/byprod")
    public List<Media> getByProductId(@RequestParam(value = "prodid") Integer prodid){
        return repo.findByProducts_Id(prodid);
    }

    /**
     * Method to get media by TypeMedia id
     * @param tmid
     * @return
     */
    @RequestMapping("/bytm")
    public List<Media> getByTypeMediaId(@RequestParam(value = "tmid") Integer tmid){
        return repo.findByTypemedia_Id(tmid);
    }

    /**
     * Method to get media by product id and typeMedia id
     * @param proid
     * @param tmid
     * @return
     */
    @RequestMapping("/byprotm")
    public List<Media> getByProductIdAndTypemediaId(@RequestParam(value = "proid") Integer proid,
                                                    @RequestParam(value = "tmid") Integer tmid){
        return repo.findByProducts_IdAndTypemedia_Id(proid, tmid);
    }
}
