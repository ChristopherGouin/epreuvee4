package fr.lisa.controllers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.Pages;
import fr.lisa.repositories.PagesRepository;

@CrossOrigin
@RestController
@RequestMapping("/pages")
public class PagesController {

    @Autowired
    public PagesRepository repo;

    /**
     * Method to get all pages
     * @return
     */
    @RequestMapping("/all")
    public List<Pages> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get a page by id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public Pages getByid(@RequestParam(value = "id") Integer id) {
        return repo.findById(id);
    }

    /**
     * Method to get pages by catalog id
     * @param catid
     * @return
     */
    @RequestMapping("/bycatalog")
    public List<Pages> getByCatalogId(@RequestParam(value = "catid") Integer catid){
        return repo.findByCatalogs_Id(catid);
    }


    @RequestMapping("/bycatalogandnum")
    public Pages getByCatalogIdAndNumber(@RequestParam(value = "catid") Integer catid,
                                        @RequestParam(value = "num") Integer num) {
        return repo.findByCatalogs_IdAndNumber(catid, num);
    }
}
