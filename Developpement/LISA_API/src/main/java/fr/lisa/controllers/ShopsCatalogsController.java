package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.ShopsCatalogs;
import fr.lisa.repositories.ShopsCatalogsRepository;

@CrossOrigin
@RestController
@RequestMapping("/shopscatalogs")
public class ShopsCatalogsController {

    @Autowired
    public ShopsCatalogsRepository repo;

    /**
     * Method to get all shopsCatalogs
     * @return
     */
    @RequestMapping("/all")
    public List<ShopsCatalogs> getall(){
        return repo.findAll();
    }

    /**
     * Method to get shopsCatalogs by shop id
     * @param shopid
     * @return
     */
    @RequestMapping("/byshops")
    public List<ShopsCatalogs> getByShops(@RequestParam(value = "shopid") Integer shopid){
        return repo.findByShops_Id(shopid);
    }

    /**
     * Method to get shopsCatalogs by catalog id
     * @param shopid
     * @return
     */
    @RequestMapping("/bycatalogs")
    public List<ShopsCatalogs> getByCatalogs(@RequestParam(value = "catid") Integer catid){
        return repo.findByCatalogs_Id(catid);
    }

    /**
     * Method to get shopsCatalogs by shop id and catalogs id
     * @param shopid
     * @return
     */
    @RequestMapping("/byshopsandcatalogs")
    public ShopsCatalogs getByShopsAndCatalogs(@RequestParam(value = "shopid") Integer shopid,
                                                    @RequestParam(value = "catid") Integer catid){
        return repo.findByShops_IdAndCatalogs_Id(shopid, catid);
    }


}
