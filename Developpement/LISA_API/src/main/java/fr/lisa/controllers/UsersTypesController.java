package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.UsersTypes;
import fr.lisa.repositories.UsersTypesRepository;

@CrossOrigin
@RestController
@RequestMapping("/userstypes")
public class UsersTypesController {

    @Autowired
    public UsersTypesRepository repo;

    /**
     *
     * @return
     */
    @RequestMapping("/all")
    public List<UsersTypes> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get a userstype by id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public UsersTypes getById(@RequestParam(value = "id") Integer id) {
        return repo.findById(id);
    }

    /**
     * Method to get userstype by label
     * @param label
     * @return
     */
    @RequestMapping("/bylabel")
    public List<UsersTypes> getByLabel(@RequestParam(value = "label") String label){
        return repo.findByLabelContains(label);
    }
}
