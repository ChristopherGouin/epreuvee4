package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.TypeMedia;
import fr.lisa.repositories.TypeMediaRepository;

@CrossOrigin
@RestController
@RequestMapping("/typemedia")
public class TypeMediaController {

    @Autowired
    public TypeMediaRepository repo;

    /**
     * Method to get all TypeMedia
     * @return
     */
    @RequestMapping("/all")
    public List<TypeMedia> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get a TypeMadia by id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public TypeMedia getById(@RequestParam(value = "id") Integer id) {
        return repo.findById(id);
    }

    /**
     * Method to get TypeMedia by label
     * @param label
     * @return
     */
    @RequestMapping("/bylabel")
    public List<TypeMedia> getBYLabel(@RequestParam(value = "label") String label){
        return repo.findByLabelContains(label);
    }
}
