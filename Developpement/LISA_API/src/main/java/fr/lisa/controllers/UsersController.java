package fr.lisa.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.Users;
import fr.lisa.repositories.UsersRepository;

@CrossOrigin
@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    public UsersRepository repo;

    /**
     * Method to get all users
     * @return
     */
    @RequestMapping("/all")
    public List<Users> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get a users by id
     * @param id
     * @return
     */
    @RequestMapping("byid")
    public Users getById(@RequestParam(value = "id") Integer id) {
        return repo.findById(id);
    }

    /**
     * Method to get users by login
     * @param login
     * @return
     */
    @RequestMapping("/bylogin")
    public List<Users> getByLogin(@RequestParam(value = "login") String login){
        return repo.findByLoginContains(login);
    }

    /**
     *  method to get users by userstypes
     * @param userstypesid
     * @return
     */
    @RequestMapping("/byuserstypes")
    public List<Users> getByUsersType(@RequestParam(value = "userstypesid") Integer userstypesid){
        return repo.findByUserstypes_Id(userstypesid);
    }

    /**
     * Method to get a user by login and password
     * @param login
     * @param password
     * @return
     */
    @RequestMapping("/byloginandpassword")
    public Users getByLoginAndPassword(@RequestParam(value = "login") String login,
                                        @RequestParam(value = "password") String password) {
        return repo.findByLoginAndPassword(login, password);
    }

    @RequestMapping("/bytoken")
    public Users getByToken(@RequestParam(value = "token") String token) {
        return repo.findByToken(token);
    }

    /**
     * Method to get user by login, password and token
     * @param login
     * @param password
     * @param token
     * @return
     */
    @RequestMapping("/byloginpasswordtoken")
    public Users getByloginpasswordtoken(@RequestParam(value = "login") String login,
                                        @RequestParam(value = "password") String password,
                                        @RequestParam(value = "token") String token) {
        return repo.findByLoginAndPasswordAndToken(login, password, token);
    }
}
