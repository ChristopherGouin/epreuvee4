package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.Shops;
import fr.lisa.repositories.ShopsRepository;

@CrossOrigin
@RestController
@RequestMapping("/shops")
public class ShopsController {

    @Autowired
    public ShopsRepository repo;

    /**
     * Method to get all shops
     * @return
     */
    @RequestMapping("/all")
    public List<Shops> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get a shop by id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public Shops getByid(@RequestParam(value = "id") Integer id) {
        return repo.findById(id);
    }

    /**
     * Method to get shops by label
     * @param label
     * @return
     */
    @RequestMapping("/bylabel")
    public List<Shops> getByLabel(@RequestParam(value = "label") String label){
        return repo.findByLabelContains(label);
    }

    /**
     * Method to get a chop by importid
     * @param importid
     * @return
     */
    @RequestMapping("/byimportid")
    public Shops getByImportId(@RequestParam(value = "importid") Integer importid) {
        return repo.findByImportid(importid);
    }
}
