package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.Products;
import fr.lisa.repositories.ProductsRepository;

@CrossOrigin
@RestController
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    public ProductsRepository repo;

    /**
     * Method to get all products
     * @return
     */
    @RequestMapping("/all")
    public List<Products> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get a product by id
     * @param id
     * @return
     */
    @RequestMapping("/byid")
    public Products getById(@RequestParam(value = "id") Integer id) {
        return repo.findById(id);
    }

    /**
     * Method to get products by catalog id
     * @param catid
     * @return
     */
    @RequestMapping("/bycategory")
    public List<Products> getByCategory(@RequestParam(value = "catid") Integer catid){
        return repo.findByCategories_Id(catid);
    }

    /**
     * Method to get products by code
     * @param code
     * @return
     */
    @RequestMapping("/bycode")
    public Products getByCode(@RequestParam(value = "code") Integer code){
        return repo.findByCode(code);
    }

    /**
     * Method to get products by label
     * @param label
     * @return
     */
    @RequestMapping("/bylabel")
    public List<Products> getByLabel(@RequestParam(value = "label") String label){
        return repo.findByLabelContains(label);
    }

    /**
     * Method to get products by catalog
     * @param label
     * @return
     */
    @RequestMapping("/bycatalog")
    public List<Products> getByCatalog(@RequestParam(value = "catid") Integer catid){
        return repo.findByCatalog(catid);
    }
}
