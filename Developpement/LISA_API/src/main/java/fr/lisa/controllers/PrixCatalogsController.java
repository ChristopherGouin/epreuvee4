package fr.lisa.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.lisa.entities.PrixCatalogs;
import fr.lisa.repositories.PrixCatalogsRepository;

@CrossOrigin
@RestController
@RequestMapping("/prixcatalogs")
public class PrixCatalogsController {

    @Autowired
    public PrixCatalogsRepository repo;

    /**
     * Method to get all prixcatalogs
     * @return
     */
    @RequestMapping("/all")
    public List<PrixCatalogs> getAll(){
        return repo.findAll();
    }

    /**
     * Method to get prixcatalogs by product id
     * @param proid
     * @return
     */
    @RequestMapping("/byprod")
    public List<PrixCatalogs> getByProductId(@RequestParam(value = "proid") Integer proid){
        return repo.findByProducts_Id(proid);
    }

    /**
     * Method to get prixcatalogs by catalog id
     * @param catid
     * @return
     */
    @RequestMapping("/bycatalog")
    public List<PrixCatalogs> getByCatlogId(@RequestParam(value = "catid") Integer catid){
        return repo.findByCatalogs_Id(catid);
    }

    /**
     * Method to get prixcatalogs by product id and catalog id
     * @param proid
     * @param catid
     * @return
     */
    @RequestMapping("/byprodandcatalog")
    public PrixCatalogs getByproductIdAndCatalogId(@RequestParam(value = "proid") Integer proid,
                                                    @RequestParam(value = "catid") Integer catid) {
        return repo.findByProducts_IdAndCatalogs_Id(proid, catid);
    }
}
