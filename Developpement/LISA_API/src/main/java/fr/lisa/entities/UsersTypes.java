package fr.lisa.entities;
// Generated 11 janv. 2018 10:29:33 by Hibernate Tools 5.2.6.Final


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * UsersTypes generated by hbm2java
 */
@Entity
@Table(name = "Userstypes", schema = "dbo", catalog = "LISA")
public class UsersTypes {

    private int id;
    private String label;

    private Set<Users> users = new HashSet<Users>(0);

    public UsersTypes() {
    }

    public UsersTypes(int id) {
        this.id = id;
    }

    public UsersTypes(int id, String label, Set<Users> users) {
        this.id = id;
        this.label = label;
        this.users = users;
    }

    @Id
    @Column(name = "Id", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "Label")
    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "userstypes")
    @JsonManagedReference
    public Set<Users> getUserses() {
        return this.users;
    }

    public void setUserses(Set<Users> users) {
        this.users = users;
    }

}
