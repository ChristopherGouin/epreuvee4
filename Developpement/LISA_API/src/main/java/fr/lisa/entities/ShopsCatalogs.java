package fr.lisa.entities;
// Generated 5 janv. 2018 09:58:03 by Hibernate Tools 5.2.6.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * ShopsCatalogs generated by hbm2java
 */
@Entity
@Table(name = "Shopscatalogs", schema = "dbo", catalog = "LISA",
uniqueConstraints = { @UniqueConstraint(columnNames={"Id_Catalogs","Id_Shops"})})
public class ShopsCatalogs {

    private Integer Id;


    private Catalogs catalogs;
    private Shops shops;
    private Date startdate;
    private Date enddate;

    public ShopsCatalogs() {
    }

    public ShopsCatalogs(Catalogs catalogs, Shops shops) {
        this.catalogs = catalogs;
        this.shops = shops;
    }

    public ShopsCatalogs(Integer Id, Catalogs catalogs, Shops shops, Date startdate, Date enddate) {
        this.Id = Id;
        this.catalogs = catalogs;
        this.shops = shops;
        this.startdate = startdate;
        this.enddate = enddate;
    }

    @Id
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Id_Catalogs", nullable = false, insertable = false, updatable = false)
    @JsonBackReference
    public Catalogs getCatalogs() {
        return this.catalogs;
    }

    public void setCatalogs(Catalogs catalogs) {
        this.catalogs = catalogs;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Id_Shops", nullable = false, insertable = false, updatable = false)
    @JsonBackReference
    public Shops getShops() {
        return this.shops;
    }

    public void setShops(Shops shops) {
        this.shops = shops;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "startdate", length = 23)
    public Date getstartdate() {
        return this.startdate;
    }

    public void setstartdate(Date startdate) {
        this.startdate = startdate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "enddate", length = 23)
    public Date getenddate() {
        return this.enddate;
    }

    public void setenddate(Date enddate) {
        this.enddate = enddate;
    }

}
