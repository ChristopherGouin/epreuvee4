angular.module('Viewer').directive('myEnter', function(){
    return function(scope, element, attrs){
        element.bind('keydown keypress', function(event){
            // Test si la touche pressée est la touche entrée (code 13)
            if (event.which === 13)
            {
                // Execute la fonction instantanément
                scope.$apply(function(){
                    // Evalue une expression passée sous forme d'une chaine de caractère
                    scope.$eval(attrs.myEnter);
                });

                // Annule la propagation de l'évènement sur les couches supérieures du DOM
                event.preventDefault();
            }
        });        
    }
});