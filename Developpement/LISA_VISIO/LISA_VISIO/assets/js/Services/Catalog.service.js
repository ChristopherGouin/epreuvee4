/*
**  Service which permit to make a link between catalogChoice and catalogDisplay
*/
angular.module('Viewer').service('CatalogSvc', function() {
    
    var catalogUsed = 'catalogUsed';
    /*
    **  Set the value of catalogUsed in sessionStorage, which mean a catalog have been choose
    */
    this.setCatalog = function(catalog) {
        sessionStorage.setItem(catalogUsed, JSON.stringify(catalog));
    }
    
    /*
    **  Get the value of catalogUsed in sessionStorage
    */
    this.getCatalog = function() {
        return JSON.parse(sessionStorage.getItem(catalogUsed));
    }
});