/*
**  Service which permit to manage data connexion from store to viewer
*/
angular.module('Viewer').service('ConnexionSvc', function() {

    //#region Variables
    var storeToken = 'storeToken';
    var storeUserId = 'storeUserId';
    //#endregion
    

    //#region Method
    /**
     * 
     * @param { Save in sessionStorage the store's token } token
     */
    this.setToken = function(token) {
        sessionStorage.setItem(storeToken, token);
    }

    /**
     * Get the token of the connected store, 
     */
    this.getToken = function() {
        return sessionStorage.getItem(storeToken);
    }

    /**
     * Save in sessionStorage the store's user id
     * @param {*} userId 
     */
    this.setUserId = function(userId) {
        sessionStorage.setItem(storeUserId, userId);
    }

    /**
     * Get the user Id of the connected store, 
     */
    this.getUserId = function() {
        return sessionStorage.getItem(storeUserId);
    }
    
    
    /**
     * Check in sessionStorage if user is connected to viewer
     */
    this.isConnected = function() {
        var id = this.getUserId();
        return (id != undefined && id > 0);
    }
    //#endregion
});