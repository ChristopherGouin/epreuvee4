/*
 **  Controller which permit to load the catalog from the DB in viewer
 */
angular.module('Viewer').controller('catalogChoiceCtrl', ['$scope', '$http', '$window', 'ConnexionSvc', 'CatalogSvc', function catalogChoiceCtrl($scope, $http, $window, ConnexionSvc, CatalogSvc) {

    

    const urlApi = "http://10.40.2.100:8888/";

    /**
     * First call from the page
     */
    $scope.init = function () {
        checkConnection();


    }

    /**
     * 
     * @param {string} userId 
     */
    var laodCatalogByUser = function (userId) {

        $http.get(urlApi + 'catalog/byuser?userid=' + userId + '&token=' + ConnexionSvc.getToken())
            .then(
                function (successResponse) {
                    $scope.userCatalogs = successResponse.data;
                },
                function (errorResponse) {
                    $scope.userCatalogs = [];
                }
            )
    }

    /**
     * Called when user click on a catalog
     * Save catalogId into sessionStorage using CatalogSvc and redirect to catalogDisplay
     * @param {*} catalogId 
     */
    $scope.ChooseCatalog = function (catalog) {
        CatalogSvc.setCatalog(catalog);
        $window.location.href = 'CatalogDisplay.html';
    }

    var checkConnection = function () {

        if (ConnexionSvc.isConnected()) { //verify the connexion
            laodCatalogByUser(ConnexionSvc.getUserId());
        } else {
            $window.location.href = 'index.html';
        }
    }

}]);