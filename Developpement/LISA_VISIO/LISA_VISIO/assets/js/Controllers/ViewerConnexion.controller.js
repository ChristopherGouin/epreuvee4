/*
 **  Controller which allow a connexion by a store to the viewer and stock token in sesssionStorage
 */
angular.module('Viewer').controller('ViewerConnexionCtrl', ['$scope', '$http', '$window', 'ConnexionSvc', function catalogChoiceCtrl($scope, $http, $window, ConnexionSvc) {

    // URL of the API
    const urlApi = "http://10.40.2.100:8888/";

    /**
     * Request API to verify if a store can be connect with the token which has been enter
     */
    $scope.callAPI = function () {
        

        $http({
            method: "GET",
            url: urlApi + "users/bytoken?token=" + $scope.token
        }).then(

            function mySuccess(response) {
                $scope.user = response.data;
                verifyToken();
            },

            function myError(response) {
                $scope.user = response.statusText;
                verifyToken();
            });
            
    }

    /*
     **  Verify if token entered is available
     */
    var verifyToken = function () {
        if ($scope.user != undefined && $scope.user != "") {
            ConnexionSvc.setToken($scope.token);
            ConnexionSvc.setUserId($scope.user.id);
            $window.location.href = './CatalogChoice.html';
        } else {
            $scope.token = "";
            $scope.status = "Token incorrect !";
        }
    }

    

}]);
