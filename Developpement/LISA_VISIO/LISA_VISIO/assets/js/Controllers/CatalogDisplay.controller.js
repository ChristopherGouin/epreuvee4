/*
 **  Controller which permit to load the catalog from the DB in viewer
 */
angular
    .module('Viewer')
    .controller('CatalogDisplayCtrl', ['$scope', '$http', '$window', '$mdDialog', 'ConnexionSvc', 'CatalogSvc', function catalogDisplayCtrl($scope, $http, $window, $mdDialog, ConnexionSvc, CatalogSvc) {


        //#region Variable






        // Catalog to display
        $scope.catalog;

        // List of category, load from API
        $scope.catalogCategories;

        // Current categorie which been selected, null if no categorie has been selected
        $scope.currentCategory = null;

        // Current number of the page which will be display
        $scope.currentNumPage = 1;

        // List of all articles
        $scope.allArticles;

        // List of current articles which will be display
        $scope.currentArticles = [];

        // Current article which would be detailled
        $scope.currentArticle;

        // Bool which mean if catalog have to be diplayed by list or by "form"
        $scope.isListDisplayed = true;

        //#endregion

        //#region Constante
        const urlApi = "http://10.40.2.100:8888/";
        //#endregion





        //#region Methods

        /*
         **  Called at the begininng of the page
         **  Call the redirection functions and the api call getCatalogId
         */
        $scope.init = function () {
            verifications();


        }


        /*
         **  Request API to get all catalogs from the connected store
         **  Call getCategories when request is done
         */
        var getCatalog = function () {
            $scope.catalog = CatalogSvc.getCatalog();


        }

        /*
         **  Request API to get all categories
         **  Call getArticles when request is done
         */
        var getCategories = function () {
            $http({
                method: "GET",
                url: urlApi + "category/all?token=" + ConnexionSvc.getToken()
            }).then(

                function mySuccess(response) {
                    $scope.catalogCategories = response.data;

                },

                function myError(response) {
                    $scope.catalogCategories = response.statusText;
                });
        }

        /**
         * Request API to get all articles
         */
        var getArticles = function () {
            $http({
                method: "GET",
                url: urlApi + "products/all?token=" + ConnexionSvc.getToken()
            }).then(

                function mySuccess(response) {
                    $scope.allArticles = response.data;
                    getCategories();
                    loadArticlesFromCurrentPage();
                },

                function myError(response) {
                    $scope.catalogArticles = response.statusText;
                });
        }




        // #########################################################
        // ###############  ARTICLES - PAGE GESTION  ###############
        // #########################################################


        /*
         **  Check if numTmp is an available page number, if true load the articles
         */
        $scope.loadArticlesFromNumPage = function (numTmp) {
            if (isNumPageCorrect(numTmp)) {
                $scope.currentNumPage = numTmp;
                loadArticlesFromCurrentPage();
            }

        }

        var isNumPageCorrect = function (numPageToCheck) {
            return (1 <= numPageToCheck && numPageToCheck <= $scope.catalog.pages.length);
        }

        /**
         * Load a page from the current page number
         */
        var initCurrentPage = function () {
            $scope.currentPage = $scope.catalog.pages.find(page => page.number === $scope.currentNumPage);
        }

        /*
         **  Load all articles from the current page
         */
        var loadArticlesFromCurrentPage = function () {

            var articles = [];
            // Init $scope.currentArticles
            $scope.currentArticles = [];

            initCurrentPage();

            // For each article in catalogArticles
            for (var article in $scope.allArticles) {

                // For each pageArticle in currentPage.pageArticles
                for (var pagesProduct in $scope.currentPage.pagesProducts) {

                    // Check if they got the same pageArticles id
                    if ($scope.allArticles[article].pagesProducts[0].id == $scope.currentPage.pagesProducts[pagesProduct].id) {
                        // Add the articles to the currentArticles to display
                        articles.push($scope.allArticles[article]);
                    }
                }
            }
            $scope.currentArticles = articles;
        }

        /*
         **  Function which define the value of current Article
         */
        $scope.defineCurrentArticle = function (article) {
            $scope.currentArticle = article;

        }





        // ###############################################################
        // ###############  ARTICLES / CATEGORIES GESTION  ###############
        // ###############################################################

        $scope.loadArticlesFromCurrentCategory = function (category) {

            if ($scope.currentCategory == category) {
                $scope.currentCategory = null;

                loadArticlesFromCurrentPage();

            } else {
                // Init $scope.currentArticles


                $scope.currentCategory = category;

                $scope.currentArticles = $scope.currentCategory.products;

            }
        }



        // #############################################
        // ###############  REDIRECTION  ###############
        // #############################################

        /*
         **  Check if there is a store connected, if not, redirect to viewerConnexion page
         */
        var verifications = function () {

            if (ConnexionSvc.getToken() == null || ConnexionSvc.getToken() == "") { //verify the connexion
                $window.location.href = 'index.html';
            } else if (CatalogSvc.getCatalog() == null) { //verify is a catalog has been choose
                $window.location.href = 'CatalogChoice.html';
            } else { // if all it's OK, then load catalog
                getArticles();
                getCatalog();
            }
        }


        //#endregion

    }]);