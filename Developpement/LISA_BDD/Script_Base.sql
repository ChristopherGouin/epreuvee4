/*------------------------------------------------------------
*        Script SQLSERVER 
------------------------------------------------------------*/

/*------------------------------------------------------------		
*       DATABASE creation		
------------------------------------------------------------*/		

IF EXISTS (SELECT * FROM sys.databases WHERE sys.databases.name = 'LISA')
BEGIN 
	DROP DATABASE LISA;
END
GO
CREATE DATABASE LISA;		
GO		
	
USE LISA;		
GO

/*------------------------------------------------------------
-- Table: Products
------------------------------------------------------------*/
CREATE TABLE Products(
	Id                INT IDENTITY (1,1) NOT NULL ,
	ImportId          INT  NOT NULL ,
	Code              INT   ,
	Label             Nvarchar (255)  ,
	Description       Nvarchar (255)  ,
	Mention           Nvarchar (255)  ,
	Packaging         Nvarchar (255)  ,
	Color             Nvarchar (255)  ,
	StartValidityDate DATETIME  ,
	EndValidityDate   DATETIME  ,
	Origin            Nvarchar (255)  ,
	Id_Categories     INT   ,
	CONSTRAINT prk_constraint_Products PRIMARY KEY NONCLUSTERED (Id)
);

CREATE UNIQUE INDEX UX_products_ImportId ON Products(ImportId);  

/*------------------------------------------------------------
-- Table: TypeMedia
------------------------------------------------------------*/
CREATE TABLE TypeMedia(
	Id    INT IDENTITY (1,1) NOT NULL ,
	Label Nvarchar (255)  ,
	CONSTRAINT prk_constraint_TypeMedia PRIMARY KEY NONCLUSTERED (Id)
);


/*------------------------------------------------------------
-- Table: Categories
------------------------------------------------------------*/
CREATE TABLE Categories(
	Id    INT IDENTITY (1,1) NOT NULL ,
	Label Nvarchar (255)  ,
	CONSTRAINT prk_constraint_Categories PRIMARY KEY NONCLUSTERED (Id)
);


/*------------------------------------------------------------
-- Table: Pages
------------------------------------------------------------*/
CREATE TABLE Pages(
	Id       INT IDENTITY (1,1) NOT NULL ,
	ImportId INT   ,
	Number   INT   ,
	Id_Catalogs INT   ,
	CONSTRAINT prk_constraint_Pages PRIMARY KEY NONCLUSTERED (Id)
);

CREATE UNIQUE INDEX UX_Pages_ImportId ON Pages(ImportId);

/*------------------------------------------------------------
-- Table: Catalogs
------------------------------------------------------------*/
CREATE TABLE Catalogs(
	Id            INT IDENTITY (1,1) NOT NULL ,
	Type          Nvarchar (255)  ,
	Label         Nvarchar (255)  ,
	Speed         Nvarchar (255)  ,
	Width         INT   ,
	Height        INT   ,
	ImportId      INT   ,
	Id_Operations INT   ,
	CONSTRAINT prk_constraint_Catalogs PRIMARY KEY NONCLUSTERED (Id)
);

CREATE UNIQUE INDEX UX_Catalogs_ImportId ON Catalogs(ImportId);

/*------------------------------------------------------------
-- Table: Shops
------------------------------------------------------------*/
CREATE TABLE Shops(
	Id       INT IDENTITY (1,1) NOT NULL ,
	Label    Nvarchar (25)  ,
	ImportId INT   ,
	CONSTRAINT prk_constraint_Shops PRIMARY KEY NONCLUSTERED (Id)
);

CREATE UNIQUE INDEX UX_Shops_ImportId ON Shops(ImportId);

/*------------------------------------------------------------
-- Table: Users
------------------------------------------------------------*/
CREATE TABLE Users(
	Id       INT IDENTITY (1,1) NOT NULL ,
	Login    Nvarchar (255) NOT NULL UNIQUE,
	Password Nvarchar (255) NOT NULL UNIQUE,
	UserType INT   ,
	CONSTRAINT prk_constraint_Users PRIMARY KEY NONCLUSTERED (Id)
);


/*------------------------------------------------------------
-- Table: Applications
------------------------------------------------------------*/
CREATE TABLE Applications(
	Id    INT IDENTITY (1,1) NOT NULL ,
	Label Nvarchar (255)  ,
	CONSTRAINT prk_constraint_Applications PRIMARY KEY NONCLUSTERED (Id)
);


/*------------------------------------------------------------
-- Table: Operations
------------------------------------------------------------*/
CREATE TABLE Operations(
	Id        INT IDENTITY (1,1) NOT NULL ,
	ImportId  INT   ,
	Code      Nvarchar (255)  ,
	Title     Nvarchar (255)  ,
	StartDate DATETIME  ,
	EndDate   DATETIME  ,
	CONSTRAINT prk_constraint_Operations PRIMARY KEY NONCLUSTERED (Id)
);

CREATE UNIQUE INDEX UX_Operations_ImportId ON Operations(ImportId);

/*------------------------------------------------------------
-- Table: Media
------------------------------------------------------------*/
CREATE TABLE Media(
	Id			 INT IDENTITY (1,1) NOT NULL ,
	Link         Nvarchar (255)  ,
	Id_Products  INT  NOT NULL ,
	Id_TypeMedia INT  NOT NULL ,
	CONSTRAINT prk_constraint_Media PRIMARY KEY NONCLUSTERED (Id)
);


/*------------------------------------------------------------
-- Table: PagesProducts
------------------------------------------------------------*/
CREATE TABLE PagesProducts(
	Id		 INT IDENTITY (1,1) NOT NULL,
	Coordx   INT   ,
	Coordy   INT   ,
	Width    INT   ,
	Heigth   INT   ,
	Id_Products   INT  NOT NULL ,
	Id_Pages INT  NOT NULL ,
	CONSTRAINT prk_constraint_PagesProducts PRIMARY KEY NONCLUSTERED (Id_Products,Id_Pages)
);


/*------------------------------------------------------------
-- Table: PrixCatalogs
------------------------------------------------------------*/
CREATE TABLE PrixCatalogs(
	ID				  INT IDENTITY (1,1) NOT NULL,
	Price             DECIMAL(6,2)   ,
	PriceBeforeCoupon DECIMAL(6,2)   ,
	PriceCrossed      DECIMAL(6,2)   ,
	ReductionEuro     DECIMAL(6,2)   ,
	ReductionPourcent DECIMAL(6,2)   ,
	AvantageEuro      DECIMAL(6,2)   ,
	AvantagePourcent  DECIMAL(6,2)   ,
	Ecotaxe           DECIMAL(6,2)   ,
	Lowerprice        DECIMAL(6,2)   ,
	Id_Catalogs       INT  NOT NULL ,
	Id_Products       INT  NOT NULL ,
	CONSTRAINT prk_constraint_PrixCatalogs PRIMARY KEY NONCLUSTERED (Id_Catalogs,Id_Products)
);


/*------------------------------------------------------------
-- Table: ShopsCatalogs
------------------------------------------------------------*/
CREATE TABLE ShopsCatalogs(
	ID			INT IDENTITY (1,1) NOT NULL,
	StartDate   DATETIME  ,
	EndDate     DATETIME  ,
	Id_Shops    INT  NOT NULL ,
	Id_Catalogs INT  NOT NULL ,
	CONSTRAINT prk_constraint_ShopsCatalogs PRIMARY KEY NONCLUSTERED (Id_Shops,Id_Catalogs)
);


/*------------------------------------------------------------
-- Table: Rights
------------------------------------------------------------*/
CREATE TABLE Rights(
	Id				INT IDENTITY (1,1) NOT NULL,
	Reading         bit   ,
	Writing         bit   ,
	Deleting        bit   ,
	Id_Users        INT  NOT NULL ,
	Id_Applications INT  NOT NULL ,
	CONSTRAINT prk_constraint_Right PRIMARY KEY NONCLUSTERED (Id_Users,Id_Applications)
);


/*------------------------------------------------------------
-- Table: UsersShops
------------------------------------------------------------*/
CREATE TABLE UsersShops(
	ID INT IDENTITY NOT NULL,
	Id_Shops INT  NOT NULL ,
	Id_Users INT  NOT NULL ,
	CONSTRAINT prk_constraint_UsersShops PRIMARY KEY NONCLUSTERED (Id_Shops,Id_Users)
);

CREATE UNIQUE INDEX UX_UsersShop ON UsersShops (ID);


CREATE TABLE UsersTypes(
	Id				INT IDENTITY (1,1) NOT NULL,
	Label			Nvarchar(255)   ,

	CONSTRAINT prk_constraint_UsersTypes PRIMARY KEY NONCLUSTERED (Id)
);

ALTER TABLE Users ADD CONSTRAINT FK_Users_Id_UsersTypes FOREIGN KEY (UserType) REFERENCES UsersTypes(Id);



ALTER TABLE Products ADD CONSTRAINT FK_Products_Id_Categories FOREIGN KEY (Id_Categories) REFERENCES Categories(Id);
ALTER TABLE Pages ADD CONSTRAINT FK_Pages_Id_Catalogs FOREIGN KEY (Id_Catalogs) REFERENCES Catalogs(Id) ON DELETE CASCADE;
ALTER TABLE Catalogs ADD CONSTRAINT FK_Catalogs_Id_Operations FOREIGN KEY (Id_Operations) REFERENCES Operations(Id) ON DELETE CASCADE;
ALTER TABLE Media ADD CONSTRAINT FK_Media_Id FOREIGN KEY (Id_Products) REFERENCES Products(Id) ON DELETE CASCADE;
ALTER TABLE Media ADD CONSTRAINT FK_Media_Id_TypeMedia FOREIGN KEY (Id_TypeMedia) REFERENCES TypeMedia(Id);
ALTER TABLE PagesProducts ADD CONSTRAINT FK_PagesProducts_Id FOREIGN KEY (Id_Products) REFERENCES Products(Id) ON DELETE CASCADE;
ALTER TABLE PagesProducts ADD CONSTRAINT FK_PagesProducts_Id_Pages FOREIGN KEY (Id_Pages) REFERENCES Pages(Id) ON DELETE CASCADE;
ALTER TABLE PrixCatalogs ADD CONSTRAINT FK_PrixCatalogs_Id FOREIGN KEY (Id_Catalogs) REFERENCES Catalogs(Id) ON DELETE CASCADE;
ALTER TABLE PrixCatalogs ADD CONSTRAINT FK_PrixCatalogs_Id_Products FOREIGN KEY (Id_Products) REFERENCES Products(Id) ON DELETE CASCADE;
ALTER TABLE ShopsCatalogs ADD CONSTRAINT FK_ShopsCatalogs_Id FOREIGN KEY (Id_Shops) REFERENCES Shops(Id) ON DELETE CASCADE;
ALTER TABLE ShopsCatalogs ADD CONSTRAINT FK_ShopsCatalogs_Id_Catalogs FOREIGN KEY (Id_Catalogs) REFERENCES Catalogs(Id) ON DELETE CASCADE;
ALTER TABLE Rights ADD CONSTRAINT FK_Right_Id FOREIGN KEY (Id_Users) REFERENCES Users(Id);
ALTER TABLE Rights ADD CONSTRAINT FK_Right_Id_Applications FOREIGN KEY (Id_Applications) REFERENCES Applications(Id);
ALTER TABLE UsersShops ADD CONSTRAINT FK_UsersShops_Id FOREIGN KEY (Id_Shops) REFERENCES Shops(Id);
ALTER TABLE UsersShops ADD CONSTRAINT FK_UsersShops_Id_Users FOREIGN KEY (Id_Users) REFERENCES Users(Id);
